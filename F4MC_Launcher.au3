#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
	#AutoIt3Wrapper_Version=P
	#AutoIt3Wrapper_Outfile=F4MC_Launcher.exe
	#AutoIt3Wrapper_UseX64=Y
	#AutoIt3Wrapper_Res_Language=1033
	#AutoIt3Wrapper_AU3Check_Parameters=-q -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %out%, %out%, IconGroup, 162,
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %out%, %out%, IconGroup, 164,
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %out%, %out%, IconGroup, 169,
	#AutoIt3Wrapper_Run_Tidy=y
	#Tidy_Parameters=/reel /ri /sci 0 /sf
	#AutoIt3Wrapper_Run_Au3Stripper=Y
	#Au3Stripper_Parameters=/so /mi 9
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#Region Pragma
	#pragma compile(Icon, Include\F4MC_Launcher.ico)
	#pragma compile(ExecLevel, asInvoker)
	#pragma compile(UPX, False)
	#pragma compile(AutoItExecuteAllowed, False)
	#pragma compile(Compression, 0)
	#pragma compile(FileDescription, Fallout 4 Mod Combiner Launcher)
	#pragma compile(ProductName, F4MC)
	#pragma compile(ProductVersion, 0.1)
	#pragma compile(FileVersion, 0.1.0.5)
	#pragma compile(LegalCopyright, � dr.)
#EndRegion Pragma

#Region Main
	If @Compiled Then
		If WinExists(@ScriptName) Or ProcessExists('F4MC.exe') Then ProcessClose('F4MC.exe')
		AutoItWinSetTitle(@ScriptName)
		DirCreate(@ScriptDir & '\Temp\')
		If FileExists(@ScriptDir & '\Locale\') Then DirMove(@ScriptDir & '\Locale\', @ScriptDir & '\Lang\')
		If FileExists(@ScriptDir & '\Graphics\') Then DirMove(@ScriptDir & '\Graphics\', @ScriptDir & '\Pics\')
		FileInstall('x64\F4MC.exe', @ScriptDir & '\F4MC.exe', 1)
		FileInstall('x64\7z.dll', @ScriptDir & '\Temp\7z.dll', 1)
		FileInstall('x64\7z.exe', @ScriptDir & '\Temp\7z.exe', 1)
		FileInstall('x64\sqlite3_x64.dll', @ScriptDir & '\Temp\sqlite3_x64.dll', 1)
		FileInstall('Include\F4MC_Logo.jpg', @ScriptDir & '\Temp\F4MC_Logo.jpg', 1)
		FileInstall('Include\F4MC_Stripe.png', @ScriptDir & '\Temp\F4MC_Stripe.png', 1)
		FileInstall('Include\F4MC_Loading.gif', @ScriptDir & '\Temp\F4MC_Loading.gif', 1)
		FileInstall('Include\Calibri.ttf', @ScriptDir & '\Temp\Calibri.ttf', 1)
		FileInstall('Include\Consolas.ttf', @ScriptDir & '\Temp\Consolas.ttf', 1)
		FileInstall('Include\F4MC.esp', @ScriptDir & '\Temp\F4MC.esp', 1)
		RunWait('F4MC.exe')
		FileDelete(@ScriptDir & '\F4MC.exe')
		DirRemove(@ScriptDir & '\Temp\', 1)
	EndIf
	Exit
#EndRegion Main
