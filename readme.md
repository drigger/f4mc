# What is F4MC (Fallout 4 Mod Combiner)?

F4MC is a free, fast and light-weight mod combiner for Fallout 4, designed to be as universal and user-friendly as possible.
> F4MC is a fork of my previous [Skyrim Mod Combiner](http://www.nexusmods.com/skyrim/mods/51467).  
  
> If you like what I do you can buy me a drink by clicking this pretty button here:  [![Donate with PayPal Button](http://i.imgur.com/q5Azoli.png "Donate with PayPal")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LVHX9P9F7UHHQ)
> or send a few satoshi to my BTC wallet: 1Bn9z83YvbGfNCgVHUcLcWANutucFC9nNz

---

   [![F4MC Screenshot](https://image.ibb.co/k8d3Oc/F4_MC_Screenshot.png "F4MC Screenshot")](https://image.ibb.co/k8d3Oc/F4_MC_Screenshot.png)

---

## F4MC features:

* Written fully in AutoIt, so it is **Windows-only**  
* Comes with a **[7-Zip](http://www.7-zip.org/)** implemented  
* Uses **SQLite** as an internal database  
* Works with _archived_ and/or _extracted_ mods  
* Automatically installs _compatibility patches_, if needed  
* Dynamically calculates _space required_  
* Dynamically indicates _mod availability_  
* Optionally packs the combined output into _archive_  
* Automatically creates readable _detailed log_  

---

## Downloading & updating:

* Latest F4MC version can always be found in **[Downloads](https://bitbucket.org/drigger/f4mc/downloads)** section  
* There is also a new **[F4MC homepage on Nexus](https://www.nexusmods.com/fallout4/mods/12063)** available  
* Or you can simply click **[here](https://bitbucket.org/drigger/f4mc/downloads/F4MC_0.1.0.5.7z)**  
* And don't worry, F4MC will update itself once installed (but you can still check this page every now and then, just in case)  

---

## Issues:

If you find an issue, please report it on the **[issue tracker](https://bitbucket.org/drigger/f4mc/issues)**.  
However, please use the search function to make sure you don't make a duplicate.  
  
Creating a new issue:  

* Make a coherent description that explains your issue in details  
* Use of pictures is welcomed to better describe your issue  
* Please make sure you've included your faulty compilation log file (it should be available in the corresponding _Logs_ folder)  

---

## Thanks for donations from:

* Chris Soldatos  
* Jeffrey Smith  
* John Spurgeon  
* Joshua Baker  
* Luc Bobko  
* Malcolm Allison  
* Martin Klinge  
* Matthew Dayne  
* Mohammed Muhanna  
* Pascal Pascher  
* Sergio Bonfiglio  
* Tomas Sanchez Gutierrez  
  
> Really, thank you all very much, guys!  

---

## Latest changes:

> * [0.1.0.5 @ 2018.05.19]  
(-) Parts of obsolete optimize feature  

> * [0.1.0.4 @ 2018.05.02]  
(+) Tooltips for mods with additional info  
(U) 7-Zip updated to the latest 18.05  

> * [0.1.0.3 @ 2018.04.27]  
(First Public Beta)  

> * [0.1.0.2 @ 2016.07.20]  
(Third Public Alpha)  

> * [0.1.0.1 @ 2015.11.25]  
(Second Public Alpha)  

> * [0.1.0.0 @ 2015.11.19]  
(First Public Alpha)  